---

#############################################################
# Unfortunately the version tag needs to be repeated
# in multiple places here because variables are not supported
# in the include section
# https://gitlab.com/gitlab-org/gitlab-ce/issues/65621

include:
  - project: "gitlab-com/gl-infra/k8s-workloads/common"
    file: "/ci/version-checks.yml"
    ref: 'v4.0.0'
  - project: "gitlab-com/gl-infra/k8s-workloads/common"
    file: "/ci/deploy.yml"
    ref: 'v4.0.0'
  - project: "gitlab-com/gl-infra/k8s-workloads/common"
    file: "/ci/except-com.yml"
    ref: 'v4.0.0'
  - project: "gitlab-com/gl-infra/k8s-workloads/common"
    file: "/ci/except-ops.yml"
    ref: 'v4.0.0'
  - project: "gitlab-com/gl-infra/k8s-workloads/common"
    file: "/ci/cluster-init-before-script.yml"
    ref: 'v4.0.0'

  # TODO re-enable after https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/859
  # - template: Container-Scanning.gitlab-ci.yml

variables:
  CI_IMAGE_VERSION: 'v4.0.0'

#############################################################

.pre-base:
  variables:
    PROJECT: gitlab-pre
  environment:
    url: https://pre.gitlab.com

.pre:
  extends:
    - .pre-base
  variables:
    CLUSTER: pre-gitlab-gke
    REGION: us-east1
  environment:
    name: pre

#### OPS

.ops:
  variables:
    PROJECT: gitlab-ops
    CLUSTER: ops-gitlab-gke
    REGION: us-east1
  environment:
    name: ops
    url: https://ops.gitlab.net

#### GSTG

.gstg-base:
  variables:
    PROJECT: gitlab-staging-1
  environment:
    url: https://staging.gitlab.com

.gstg:
  extends:
    - .gstg-base
  variables:
    CLUSTER: gstg-gitlab-gke
    REGION: us-east1
  environment:
    name: gstg
  resource_group: gstg

.gstg-us-east1-b:
  extends:
    - .gstg-base
  variables:
    CLUSTER: gstg-us-east1-b
    REGION: us-east1-b
  environment:
    name: gstg-us-east1-b
  resource_group: gstg-us-east1-b

.gstg-us-east1-c:
  extends:
    - .gstg-base
  variables:
    CLUSTER: gstg-us-east1-c
    REGION: us-east1-c
  environment:
    name: gstg-us-east1-c
  resource_group: gstg-us-east1-c

.gstg-us-east1-d:
  extends:
    - .gstg-base
  variables:
    CLUSTER: gstg-us-east1-d
    REGION: us-east1-d
  environment:
    name: gstg-us-east1-d
  resource_group: gstg-us-east1-d

#### GPRD

.gprd-base:
  variables:
    PROJECT: gitlab-production
  environment:
    url: https://gitlab.com

.gprd:
  extends:
    - .gprd-base
  variables:
    CLUSTER: gprd-gitlab-gke
    REGION: us-east1
  environment:
    name: gprd
  resource_group: gprd

.gprd-us-east1-b:
  extends:
    - .gprd-base
  variables:
    CLUSTER: gprd-us-east1-b
    REGION: us-east1-b
  environment:
    name: gprd-us-east1-b
  resource_group: gprd-us-east1-b

.gprd-us-east1-c:
  extends:
    - .gprd-base
  variables:
    CLUSTER: gprd-us-east1-c
    REGION: us-east1-c
  environment:
    name: gprd-us-east1-c
  resource_group: gprd-us-east1-c

.gprd-us-east1-d:
  extends:
    - .gprd-base
  variables:
    CLUSTER: gprd-us-east1-d
    REGION: us-east1-d
  environment:
    name: gprd-us-east1-d
  resource_group: gprd-us-east1-d

#### ORG-CI

.org-ci:
  variables:
    PROJECT: gitlab-org-ci-0d24e2
    CLUSTER: org-ci-gitlab-gke
    REGION: us-east1
  environment:
    name: org-ci

stages:
  - check
  - diff
  - apply

#########################################
# Attempts to find an MR on .com and add
# a link to the pipeline that runs on ops

notify_com_mr:
  image: "${CI_REGISTRY}/gitlab-com/gl-infra/k8s-workloads/common/notify-mr:${CI_IMAGE_VERSION}"
  extends:
    - .except-com
  stage: check
  script:
    # Send a pipeline start nofication to the MR on .com with
    # a link to ops
    - notify-mr -s
  # It is possible that this job will fail if a branch is created
  # without a corresponding MR, for this reason we allow it to
  # fail
  allow_failure: true

### Job Templates

.lint: &lint
  image: "${CI_REGISTRY}/gitlab-com/gl-infra/k8s-workloads/common/k8-helm-ci:${CI_IMAGE_VERSION}"
  stage: check
  script:
    - gcloud auth activate-service-account --key-file $SERVICE_KEY_RO
    # Configure SSH authentication for the helm-git plugin
    - chmod 400 "$SSH_PRIVATE_KEY"
    - export GIT_SSH_COMMAND="ssh -i $SSH_PRIVATE_KEY -o IdentitiesOnly=yes -o GlobalKnownHostsFile=$SSH_KNOWN_HOSTS"
    # `helmfile lint` does not sync repos for releases that are not installed in
    # the given environment. Since we run lint jobs for every
    # environment/release combination, we need to explicitly sync the repos
    # first. This allows the chart to be linted even if it is not used in an
    # environment. This is un-necessary, but probably better than writing logic
    # to determine whether or not we need to do it for the given inputs.
    - helmfile -e ${CI_ENVIRONMENT_SLUG} repos
    - helmfile -e ${CI_ENVIRONMENT_SLUG} lint --skip-deps

.diff: &diff
  stage: diff
  script:
    - 'helmfile -e ${CI_ENVIRONMENT_SLUG} diff --context 10 --suppress-secrets | tee /tmp/helm-diff'
    # Send a diff notification, allowed to
    # fail so we don't block the pipeline
    - '/k8s-workloads/notify-mr -d /tmp/helm-diff -e "$CI_ENVIRONMENT_NAME" || echo "WARNING: notify-mr diff notification failed"'

.apply: &apply
  stage: apply
  script:
    - helmfile -e ${CI_ENVIRONMENT_SLUG} apply --context 10 --suppress-secrets
  variables:
    DRY_RUN: "false"
  only:
    refs:
      - master

### pre

pre:lint:
  <<: *lint
  extends:
    - .pre
    - .except-ops

pre:diff:
  <<: *diff
  extends:
    - .pre
    - .except-com
    - .deploy

pre:apply:
  <<: *apply
  extends:
    - .pre
    - .except-com
    - .deploy
  resource_group: pre

### ops

ops:lint:
  <<: *lint
  extends:
    - .ops
    - .except-ops

ops:diff:
  <<: *diff
  extends:
    - .ops
    - .except-com
    - .deploy

ops:apply:
  <<: *apply
  extends:
    - .ops
    - .except-com
    - .deploy
  resource_group: ops

### gstg

gstg:lint:
  <<: *lint
  extends:
    - .gstg
    - .except-ops

gstg-us-east1-b:lint:
  <<: *lint
  extends:
    - .gstg-us-east1-b
    - .except-ops

gstg-us-east1-c:lint:
  <<: *lint
  extends:
    - .gstg-us-east1-c
    - .except-ops

gstg-us-east1-d:lint:
  <<: *lint
  extends:
    - .gstg-us-east1-d
    - .except-ops

gstg:diff:
  <<: *diff
  extends:
    - .gstg
    - .except-com
    - .deploy

gstg-us-east1-b:diff:
  <<: *diff
  extends:
    - .gstg-us-east1-b
    - .except-com
    - .deploy

gstg-us-east1-c:diff:
  <<: *diff
  extends:
    - .gstg-us-east1-c
    - .except-com
    - .deploy

gstg-us-east1-d:diff:
  <<: *diff
  extends:
    - .gstg-us-east1-d
    - .except-com
    - .deploy

gstg:apply:
  <<: *apply
  extends:
    - .gstg
    - .except-com
    - .deploy

gstg-us-east1-b:apply:
  <<: *apply
  extends:
    - .gstg-us-east1-b
    - .except-com
    - .deploy

gstg-us-east1-c:apply:
  <<: *apply
  extends:
    - .gstg-us-east1-c
    - .except-com
    - .deploy

gstg-us-east1-d:apply:
  <<: *apply
  extends:
    - .gstg-us-east1-d
    - .except-com
    - .deploy

### gprd

gprd:lint:
  <<: *lint
  extends:
    - .gprd
    - .except-ops

gprd-us-east1-b:lint:
  <<: *lint
  extends:
    - .gprd-us-east1-b
    - .except-ops

gprd-us-east1-c:lint:
  <<: *lint
  extends:
    - .gprd-us-east1-c
    - .except-ops

gprd-us-east1-d:lint:
  <<: *lint
  extends:
    - .gprd-us-east1-d
    - .except-ops

gprd:diff:
  <<: *diff
  extends:
    - .gprd
    - .except-com
    - .deploy

gprd-us-east1-b:diff:
  <<: *diff
  extends:
    - .gprd-us-east1-b
    - .except-com
    - .deploy

gprd-us-east1-c:diff:
  <<: *diff
  extends:
    - .gprd-us-east1-c
    - .except-com
    - .deploy

gprd-us-east1-d:diff:
  <<: *diff
  extends:
    - .gprd-us-east1-d
    - .except-com
    - .deploy

gprd:apply:
  <<: *apply
  extends:
    - .gprd
    - .except-com
    - .deploy
  when: manual

gprd-us-east1-b:apply:
  <<: *apply
  extends:
    - .gprd-us-east1-b
    - .except-com
    - .deploy
  when: manual

gprd-us-east1-c:apply:
  <<: *apply
  extends:
    - .gprd-us-east1-c
    - .except-com
    - .deploy
  when: manual

gprd-us-east1-d:apply:
  <<: *apply
  extends:
    - .gprd-us-east1-d
    - .except-com
    - .deploy
  when: manual

### org-ci

org-ci:lint:
  <<: *lint
  extends:
    - .org-ci
    - .except-ops

org-ci:diff:
  <<: *diff
  extends:
    - .org-ci
    - .except-com
    - .deploy

org-ci:apply:
  <<: *apply
  extends:
    - .org-ci
    - .except-com
    - .deploy
  resource_group: org-ci

########################################
# Registry container scanning

# TODO re-enable after https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/859
# Imported container_scanning base job, overridden to never run.
# container_scanning:
#   stage: check
#   variables:
#     GIT_STRATEGY: fetch
#   except:
#     - branches

# plantuml:container_scanning:
#   variables:
#     CI_APPLICATION_REPOSITORY: plantuml/plantuml-server
#     CI_APPLICATION_TAG: jetty-v1.2020.7

#     # The inherited job defaults these to repo's registry credentials. Setting
#     # these to empty string prevents those credentials being used to log into
#     # Dockerhub, failing the build.
#     DOCKER_USER: ""
#     DOCKER_PASSWORD: ""
#   extends:
#     - container_scanning
#     - .except-ops
