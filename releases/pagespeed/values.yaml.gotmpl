---
deployment:
  image:
    repository: "foomo/pagespeed_exporter"
    tag: "latest"
  ports:
    - name: "metrics"
      containerPort: 9271
  probes:
    readiness:
      httpGet:
        port: "metrics"
        ## We cannot use the /metrics endpoint here
        ## due to long scrape durations for this exporter
        path: "/"
  resources:
    requests:
      cpu: 250m
      memory: 256Mi
    limits:
      memory: 1Gi
  containerCmd: ["/bin/sh"]
  containerArgs:
    - "-c"
    - |
      /bin/pagespeed_exporter \
      -api-key=$GOOGLE_API_KEY \
      {{- range $_, $target := .Values.pagespeed.targets }}
      -t={{ $target }} \
      {{- end }}
service:
  enabled: true
  headless: true
  ports:
    - name: "metrics"
      targetPort: "metrics"
      port: 9271

prometheus_monitoring:
  enabled: true
  # These values are purposely set very high due
  # to how the pagespeed exporter works, which will
  # run a test on every scrape that will take about
  # 2 minutes to run
  scrape_interval: "900s"  # 15 minutes
  scrape_timeout: "600s"    # 10 minutes

secrets:
  values:
{{ if .Values | getOrNil "local_secrets" | default false -}}
{{ readFile "private/secrets.yaml" | indent 4 -}}
{{- else -}}
{{ exec "bash" (list "-c" "gsutil cat gs://gitlab-configs/pagespeed_exporter_secret.yaml.enc | gcloud --project gitlab-ops kms decrypt --location global --keyring=gitlab-shared-configs --key config --ciphertext-file=- --plaintext-file=-") | indent 4 -}}
{{- end -}}
