# pubsubbeat

The topics referenced by this release must already exist. We manage these in
terraform. The subscriptions to the topics are lazily created by the beat
software itself.

Unless overridden, each beat deployment will try to write to an Elasticsearch
index with the same name as the topic. That index must already exist in the
Elasticsearch cluster. See
https://gitlab.com/gitlab-com/runbooks/-/tree/master/elastic for management of
Elasticsearch.
