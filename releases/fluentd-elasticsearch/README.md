# fluentd-elasticsearch release

## Development

### minikube

You'll need sufficient CPU/memory in your minikube VM to run
elasticsearch and kibana:

```
minikube start --cpus 4 --memory 4G
```
