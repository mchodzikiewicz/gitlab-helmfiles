# PlantUML

This chart deploys the PlantUML service that provides PlantUML diagrams for GitLab.com.

## Workstation setup for the oncall

- Follow the setup instructions [workstation setup for the oncall](https://gitlab.com/gitlab-com/runbooks/blob/master/docs/uncategorized/k8s-gitlab-operations.md#workstation-setup-for-the-oncall)
- Ensure you can query Kubernetes plantuml namespace

## Application Upgrading

To upgrade PlantUML submit an MR that updates the [tag version in the chart](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/blob/397adabd9cf10c171e4053628c40539a94b0d505/releases/plantuml/values.yaml#L5).
It's best to first bump the version on non-prod environments and test PlantUML rendering before upgrade.

## Monitoring and Troubleshooting

* There are multiple dashboards for monitoring both the GKE cluster and performance of the application:
  * [Production Pods](https://dashboards.gitlab.net/d/kubernetes-resources-pod/kubernetes-compute-resources-pod?orgId=1&refresh=10s&var-datasource=Global&var-cluster=gprd-gitlab-gke&var-namespace=plantuml)
  * [Production overview](https://dashboards.gitlab.net/d/plantuml-main/plantuml-overview?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gprd&var-cluster=gprd-gitlab-gke)

## CDN and Caching

All images are cached aggressively at the L7 LB which provides a CDN. Under some circumstances, it may be necessary to send a cache invalidation, this is done using the GCP console.

Example for pre-production: https://console.cloud.google.com/net-services/cdn/list?project=gitlab-pre&cdnOriginsTablesize=50

**Note**: To reset the cache for all diagrams, send a cache invalidation for `/png/*`

## HTTPs Certificate

PlantUML is configured with a [google managed certificate](https://cloud.google.com/load-balancing/docs/ssl-certificates/google-managed-certs) which is automatically renewed.
