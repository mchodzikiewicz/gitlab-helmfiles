# gitlab-ci-accounts

Binds the automation service accounts used in CI to the RBAC permissions they
need. This release must initially be applied using a role with sufficient
privilege (e.g.  from your laptop using your gcloud account), in order to imbue
the service accounts with sufficient privilege for CI pipelines to do all
subsequent deploys.

For this reason, we might want to move this bootstrapping step into Terraform so
that pipelines can be used straight away for new clusters.
