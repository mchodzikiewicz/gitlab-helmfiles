# consul release

## External Requirements

### Minikube

There are no external requirements for minikube

### GitLab.com environments

* For fetching secrets gkms and jq are required with access permissions to the gkms vault
* The gossip key is fetched from Chef, this should eventually be moved to gkms https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/10906

## Development

For the configuration in minikube:

* TLS is disabled so there are no secrets or the gossip key
* The server is also enabled where when running in prod and non-prod environments the consul server is running on VMs

Run with the minikube Helmfile environment, e.g. `helmfile -e minikube apply`
