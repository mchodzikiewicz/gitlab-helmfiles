---

{{- $storage_class := ternary "standard-regional-east1" "standard" (eq (.Environment.Values | getOrNil "region") "us-east1") }}

prometheus:
  prometheusSpec:
    containers:
      - name: prometheus
        env:
          - name: JAEGER_DISABLED
            value: "false"
          - name: JAEGER_SAMPLER_TYPE
            value: "probabilistic"
          - name: JAEGER_SAMPLER_PARAM
            value: "0.01"
          - name: JAEGER_AGENT_HOST
            valueFrom:
              fieldRef:
                fieldPath: status.hostIP
    replicas: 2
    storageSpec:
      volumeClaimTemplate:
        spec:
          storageClassName: {{ $storage_class }}
          resources:
            requests:
              storage: 200Gi
    resources:
      requests:
        cpu: "2"
        memory: 10Gi
  ingress:
    hosts:
      - "prometheus-gke.{{ .Environment.Name }}.gitlab.net"
    paths:
      - /*
  service:
   loadBalancerIP: {{ exec "gcloud" (list "--project" .Environment.Values.google_project "compute" "addresses" "list" "--filter" (printf "name=prometheus-gke-%s" .Environment.Name) "--format" "value(address)") }}
