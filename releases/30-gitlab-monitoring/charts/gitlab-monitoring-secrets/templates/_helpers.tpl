{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "gitlab-monitoring-secrets.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "gitlab-monitoring-secrets.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "gitlab-monitoring-secrets.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "gitlab-monitoring-secrets.labels" -}}
app.kubernetes.io/name: {{ include "gitlab-monitoring-secrets.name" . }}
helm.sh/chart: {{ include "gitlab-monitoring-secrets.chart" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{- define "gitlab-monitoring-secrets.prometheus_tlscert" -}}
{{- if and (eq .Values.prometheus_gke_tls.crt "") (eq .Values.prometheus_gke_tls.key "") -}}
{{- $cert := genSelfSignedCert "gitlab-monitoring-promethe-prometheus.monitoring.svc.cluster.local" nil nil 365 -}}
tls.crt: |
{{ $cert.Cert | indent 2 }}
tls.key: |
{{ $cert.Key | indent 2 }}
{{- else -}}
tls.crt: |
{{ .Values.prometheus_gke_tls.crt | indent 2 }}
tls.key: |
{{ .Values.prometheus_gke_tls.key | indent 2 }}
{{- end -}}
{{- end -}}

{{- define "gitlab-monitoring-secrets.alertmanager_tlscert" -}}
{{- if and (eq .Values.alertmanager_gke_tls.crt "") (eq .Values.alertmanager_gke_tls.key "") -}}
{{- $cert := genSelfSignedCert "gitlab-monitoring-promethe-alertmanager.monitoring.svc.cluster.local" nil nil 365 -}}
tls.crt: |
{{ $cert.Cert | indent 2 }}
tls.key: |
{{ $cert.Key | indent 2 }}
{{- else -}}
tls.crt: |
{{ .Values.alertmanager_gke_tls.crt | indent 2 }}
tls.key: |
{{ .Values.alertmanager_gke_tls.key | indent 2 }}
{{- end -}}
{{- end -}}
