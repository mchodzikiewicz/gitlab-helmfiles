# gitlab-monitoring

Deploys the Prometheus stack.

This helmfile deploys a few releases. The core is the [prometheus-operator helm
chart](https://github.com/helm/charts/tree/master/stable/prometheus-operator).
Part of this chart's configuration is the names of secret objects that the
operator user is supposed to create out of band. We create these objects in the
"gitlab-monitoring-secrets" release, which deploys a local chart.

## Usage

Manually create a few resources that are referenced by the secret chart:

1. A GCP Oauth client ID called "monitoring". [gstg example](https://console.cloud.google.com/apis/credentials/oauthclient/65580314219-jqstgo44nhpp9v525u5ktlqbbum791jk.apps.googleusercontent.com?project=gitlab-staging-1)
   * This will be used by the public ingress IAP
   * On the GCP project for which this is being installed into, navigate to APIs
     & Services > Credentials
   * Create Credentials > OAuth client ID
   * Application type _should_ be `Web application`
   * Name _must_ be: `monitoring`
   * Authorized JavaScript origins:
     * You _must_ have one for each endpoint, in this case, two, one for
       prometheus and one for alertmanager
     * Examples:
       * `https://prometheus-gke.ENV.gitlab.net`
       * `https://example-alertmanager-domain.gitlab.net` if Alertmanager needs
         to be deployed - which it almost definitely doesn't if your environment
         is new, since we use a single Alertmanager cluster in ops.
   * Authorized redirect URIs:
     * You _must_ have these configured, otherwise Google's IAP will through an
       HTTP422.  These are greatly dependent on the applications that this IAP
       will front.
     * Examples:
       * `https://prometheus-gke.ENV.gitlab.net/_gcp_gatekeeper/authenticate`
   * Select `Create`
   * A dialog box will open containing the following:
     * `Client ID`
     * `Client Secret`
   * These will be retrieved by helmfile at deploy time, you don't need to copy
     them now.
1. thanos-sidecar will need a service account private key that grants access to
   the environment's prometheus bucket.
    1. If the [storage-buckets](https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/storage-buckets)
       terraform module is installed for your environment, the bucket and
       service acount will already exist.
    1. Manually create a private key for the "ENV-prometheus-sa" service
       account. We don't create these in Terraform to avoid storing the private
       key in unencrypted terraform state.
    1. Create a yaml file with the following format:

       ```
       ---
       type: GCS
       config:
         bucket: gitlab-gstg-prometheus
         service_account: |-
           {
             ...
           }
       ```
    1. Encrypt the file: `gcloud --project=gitlab-ops kms encrypt --location global --keyring gitlab-shared-configs --key config --ciphertext-file=env-thanos-secret.yaml.enc --plaintext-file=env-thanos-secret.yaml`
    1. Copy the file to the secrets bucket: `gsutil cp env-thanos-secret.yaml.enc gs://gitlab-configs/env-thanos-secret.yaml.enc`
    1. Delete the yaml file and its encrypted counterpart from your local
       filesystem.
    1. In the future this flow can be modified to use workload identity.

This release supports Google-managed certificates, so you don't need to
configure TLS certificates if you are deploying onto a GKE cluster that supports
them - which you almost definitely are, if you're reading this in order to
create a new deployment.

Create a file `ENV.yaml.gotmpl` for your new environment, containing any
environment-specific overrides you might need. See existing environments to get
a sense of what you might need.

In `bases/environments.yaml`, create some configuration for your environment:

```yaml
environments:
  an-env:
    values:
      - gitlab_monitoring:

          # Most environments won't need an Alertmanager
          alertmanager:
            installed: false
            domain: "set-if-installed-is-true.gitlab.net"
            # A feature flag that creates an extra headless service for alertmanager.
            extras_installed: false

          google_managed_cert:
            enabled: true

          # A feature flag for GKE versions that use non-beta BackendConfigs.
          # Will be removed when all clusters are upgraded to 1.16
          backendconfig_api_version: "cloud.google.com/v1"

          secrets:
            # Retrieve the secrets we've manually created from GCP
            source: "gcloud"
            # Only needs to be set if the oauth client is not called "monitoring"
            oauth_name: "monitoring"
```

You should now be able to deploy this environment:

`helmfile -e ENV -l group=monitoring diff`

You should configure it in this repo's CI, as we do with all environments.

### Bootstrapping new clusters

See the "Bootstrapping" new clusters in the top-level README.md. On fresh
clusters, e.g. a new minikube/dev environment, the prometheus-operator CRDs must
be installed before charts that depend on them.

Run:

```
helmfile -e ENV -l bootstrap=true apply
```

Which installs the prometheus-operator (and its CRDs). Now you will be able to
run helmfile diffs and applies without a label filter.
