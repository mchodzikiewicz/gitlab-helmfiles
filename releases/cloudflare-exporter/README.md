# cloudflare-exporter release

## Development

Create `private/secrets.yaml` with structure:

```
CLOUDFLARE_API_EMAIL: email@domain.com
CLOUDFLARE_API_KEY: cf-api-key
```

Run with the "minikube" Helmfile environment, e.g. `helmfile -e minikube apply`.
