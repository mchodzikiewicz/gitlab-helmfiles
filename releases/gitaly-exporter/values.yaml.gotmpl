---
nameOverride: "gitaly-exporter"

deployment:
  image:
    repository: registry.gitlab.com/gitlab-org/build/cng/gitaly
    tag: v13.0.0
  containerCmd: ["/usr/local/bin/gitaly-blackbox", "/etc/configmap/exporter-config.toml"]
  configFiles:
    exporter-config.toml: |
      prometheus_listen_addr = ":9687"
      sleep = 500

      [logging]
      format = "json"

      {{- range .Values.gitaly_exporter.probes }}
      [[probe]]
      name = {{ .name | quote }}
      url = {{ .url | quote }}
      {{- end }}
  ports:
    - name: metrics
      containerPort: 9687
  probes:
    readiness:
      httpGet:
        port: "metrics"
        path: "/metrics"

  # Override in environment-specific values if necessary
  resources:
    requests:
      cpu: 256m
      memory: 256Mi
    limits:
      memory: 256Mi

service:
  enabled: true
  headless: true
  ports:
    - name: "metrics"
      targetPort: "metrics"
      port: 9687

prometheus_monitoring:
  enabled: true
  scrape_interval: "30s"
  relabelings:
    - targetLabel: type
      replacement: gitaly
    - targetLabel: tier
      replacement: stor
    - targetLabel: stage
      replacement: main
    - sourceLabels:
        - __meta_kubernetes_pod_node_name
      targetLabel: node
